<?php namespace Decoupled\Wordpress\Extension\Assets;

use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationExtension;
use Decoupled\Wordpress\Assets\AssetConfig;
use Decoupled\Wordpress\Assets\AssetQueue;

class WPAssetsExtension extends ApplicationExtension{

    public function getName()
    {
        return 'wp.assets.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        $app['$wp.assets'] = function(){

            return new AssetConfig();
        };
            
        $app['$wp.asset.queue'] = function(){

            return new AssetQueue();
        };

    }
}