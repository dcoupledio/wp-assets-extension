<?php

require('../vendor/autoload.php');

require('./mock.php');

use phpunit\framework\TestCase;
use Decoupled\Wordpress\Extension\Assets\WPAssetsExtension;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\Action\ActionExtension;
use Decoupled\Core\Extension\DependencyInjection\DependencyInjectionExtension;
use Decoupled\Wordpress\Assets\AssetConfig;

class AssetTest extends TestCase{

    public function testCanUseExtension()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new ActionExtension() );

        $app->uses( new DependencyInjectionExtension() );

        $app->uses( new WPAssetsExtension() );

        $app->uses(function( AssetConfig $wpAssets ){

            $wpAssets->add( 'jquery', 'path/to/jquery.js' );
        });

        $app->uses(function( $wpAssetQueue, $wpAssets ){

            $wpAssetQueue->add( $wpAssets );
        });

        $this->assertContains( 'jquery', WP_Mock::$regScripts );

        $this->assertContains( 'jquery', WP_Mock::$enqScripts );
    }

}